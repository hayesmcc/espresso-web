module.exports = {
  
  'Set Up' : function(client) {
	client
	.page.TestSuite().NavigatetoHomePage()
	.page.TestSuite().clickIngredientDropdown()
	
  },
  
  'Test View Ingredients' : function(client) {
	client
	.click('.mcc-view-ingredients')
	.waitForElementVisible('.mcc-ingredient-1', 1000)
    .assert.elementPresent(".mcc-ingredient-1")
    .end();
  },
	    
};
