module.exports = {
  
  'Set Up' : function(client) {
	client
	.page.TestSuite().NavigatetoHomePage()
	.page.TestSuite().clickIngredientDropdown()
  },
 
  'Test Add Ingredient' : function(client) {
	client
	.click('.mcc-add-ingredients')
	.waitForElementVisible('.addIngredientModal', 1000)
    .setValue('.newIngredientNameInput', ['Nightwatch Syrup', client.Keys.ENTER])
	.setValue('.newIngredientUnitInput', ['test', client.Keys.ENTER])
	.setValue('.newIngredientCostInput', ['10.00', client.Keys.ENTER])
	.click('.addIngredientBtn')
	.end();
  },
  


  
};
