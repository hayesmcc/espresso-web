module.exports = {
  
  'Set Up' : function(client) {
	client
	.page.TestSuite().NavigatetoHomePage()
	.page.TestSuite().viewRecipes()
	
  },
  
  'Test View Recipes' : function(client) {
	client
	.waitForElementVisible('.mcc-recipe-1', 1000)
    .assert.elementPresent(".mcc-recipe-1")
    .end();
  },
	    
};
