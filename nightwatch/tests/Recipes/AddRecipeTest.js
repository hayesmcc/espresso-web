module.exports = {
  
  'Set Up' : function(client) {
	client
	.page.TestSuite().NavigatetoHomePage()
	.page.TestSuite().clickRecipeDropdown()
  },
 
  'Test Add Recipe Item' : function(client) {
	client
	.click('.mcc-add-recipes')
	.waitForElementVisible('.addRecipeModal', 1000)
    .setValue('.newRecipeNameInput', ['Nightwatch Drink Recipe', client.Keys.ENTER])
	.setValue('.newRecipeRecipeNumberInput', ['0123', client.Keys.ENTER])
	.click('.addIngredientListBtn')
	.click('.newRecipeBtn')
	.end();
  },
  
  
};
