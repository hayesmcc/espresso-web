module.exports = {
  
  'Set Up' : function(client) {
	client
	.page.TestSuite().NavigatetoHomePage()
	.page.TestSuite().openRecipeSalesMarkupModal()
	
  },
 
  'Test Recipe Sales Markup' : function(client) {
	client
	.waitForElementVisible('.salesModal', 1000)
	.setValue('.salesMarkupInput', ['10', client.Keys.ENTER])
	.click('.recipeSalesCalculateBtn')
	.pause(1000)
	.getText('.salesHanger', function(result){
		this.assert.equal(result.value, "$1.29");
	})
	
	.end();
  }
};
