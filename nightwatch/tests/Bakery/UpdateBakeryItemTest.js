module.exports = {
  
  'Set Up' : function(client) {
	client
	.page.TestSuite().NavigatetoHomePage()
	.page.TestSuite().viewBakeryItems()
  },
 
  'Test Update Bakery Item' : function(client) {
	client
	.waitForElementVisible('.editBakeryItem2', 1000)
	.click('.editBakeryItem2')
	.waitForElementVisible('.updateBakeryNameInput', 1000)
	.clearValue('.updateBakeryNameInput')
	.setValue('.updateBakeryNameInput', 'Nightwatch Brownie Updated Test')
	.click('.updateBakeryBtn')
	.page.TestSuite().clickBakeryDropdown()
	.click('.mcc-view-bakery')
	.waitForElementVisible('.editBakeryItem2', 1000)
	.getText('.mcc-name-value2', function(result){
		this.assert.equal(result.value, 'Nightwatch Brownie Updated Test');
	})	
	.end();
  },
  
  
};
