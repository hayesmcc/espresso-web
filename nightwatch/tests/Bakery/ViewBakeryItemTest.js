module.exports = {
  
  'Set Up' : function(client) {
	client
	.page.TestSuite().NavigatetoHomePage()
	.page.TestSuite().viewBakeryItems()
	
  },
  
  'Test View Bakery Items' : function(client) {
	client
	.waitForElementVisible('.mcc-bakery-1', 1000)
    .assert.elementPresent(".mcc-bakery-1")
    .end();
  },
	    
};
