module.exports = {
  
  'Set Up' : function(client) {
	client
	.page.TestSuite().NavigatetoHomePage()
	.page.TestSuite().openBakeryItemSalesMarkupModal()
	
  },
 
  'Test Bakery Sales Markup' : function(client) {
	client
	.waitForElementVisible('.bakerySalesModal', 1000)
	.setValue('.bakerySalesMarkupInput', ['10', client.Keys.ENTER])
	.click('.bakerySalesCalculateBtn')
	.pause(1000)
	.getText('.bakerySalesHanger', function(result){
		this.assert.equal(result.value, "$2.42");
	})
	
	.end();
  }
  
  
};
