module.exports = {
  
  'Set Up' : function(client) {
	client
	.page.TestSuite().NavigatetoHomePage()
	.page.TestSuite().clickBakeryDropdown()
  },
 
  'Test Add Bakery Item' : function(client) {
	client
	.click('.mcc-add-bakery')
	.waitForElementVisible('.addBakeryModal', 1000)
    .setValue('.newBakeryNameInput', ['Nightwatch Brownie Test', client.Keys.ENTER])
	.setValue('.newBakeryVendorInput', ['test', client.Keys.ENTER])
	.setValue('.newBakeryAllergenInput', ['test', client.Keys.ENTER])
	.setValue('.newBakeryCategoryInput', ['test', client.Keys.ENTER])
	.setValue('.newBakeryCostInput', ['10.00', client.Keys.ENTER])
	.click('.newBakeryBtn')
	.end();
  },
  
  
};
