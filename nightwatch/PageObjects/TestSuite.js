module.exports = function(client) {

	this.NavigatetoHomePage = function() {
		client
			.url('http://localhost:8080/assessment-hmccardell-website/')
			.waitForElementPresent('body', 1000)

		return client;
	};

	this.viewBakeryItems = function() {
		client
			.page.TestSuite().clickBakeryDropdown()
			.click('.mcc-view-bakery')
		return client;
	};

	this.viewRecipes = function() {
		client
			.page.TestSuite().clickRecipeDropdown()
			.click('.mcc-view-recipes')
		return client;
	};

	this.clickIngredientDropdown = function() {
		client
			.waitForElementVisible('.ingredients-dropdown', 1000)
			.click('.ingredients-dropdown')
		return client;
	};

	this.clickBakeryDropdown = function() {
		client
			.waitForElementVisible('.bakery-dropdown', 1000)
			.click('.bakery-dropdown')
		return client;
	};

	this.clickRecipeDropdown = function() {
		client
			.waitForElementVisible('.recipe-dropdown', 1000)
			.click('.recipe-dropdown')
		return client;
	};

	this.openBakeryItemSalesMarkupModal = function() {
		client
			.page.TestSuite().viewBakeryItems()
			.pause(500)
			.click('.salesBakeryBtn2')

		return client;
	};
	
	this.openRecipeSalesMarkupModal = function() {
		client
			.page.TestSuite().viewRecipes()
			.pause(500)
			.click('.salesRecipeBtn1')

		return client;
	};


};