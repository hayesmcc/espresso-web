$(document).ready(function(){

	getTemplates();
	
	$(".mcc-navbar-display").on("click", ".mcc-viewIngredientsBtn", function() {
		compileBlocks ("#mcc-ingredient-template", blockTemplatesURL, ingredientURL);
	});
	
	$(".mcc-navbar-display").on("click", ".mcc-viewBakeryBtn", function() {
		compileBlocks ("#mcc-bakery-template", blockTemplatesURL, bakeryURL);
	});
	
	$(".mcc-navbar-display").on("click", ".mcc-viewRecipesBtn", function() {
		compileBlocks ("#mcc-recipe-template", blockTemplatesURL, recipeURL);
	});
	
	$(".mcc-navbar-display").on("click", ".mcc-clear-button", function() {
		clearDisplayArea();
	});
	
	$(document).on('hide.bs.modal', function() {
		clearModalInputs();
		clearIngredientsListArray();
	});
});
