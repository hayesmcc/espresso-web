function succeessToast(message) {
	toastr.options = {
	  "newestOnTop": true,
	  "positionClass": "toast-top-full-width",
	};
	toastr.success(message, 'SUCCESS');
}

function errorToast(message) {
	toastr.options = {
	  "newestOnTop": true,
	  "positionClass": "toast-top-full-width",
	};
	toastr.error(message, 'ERROR');
}

function clearModalInputs() {
	$(".input").val("");
	$(".bakerySalesHanger").text("");
	$(".salesHanger").text("");
	
	var myNode2 = document.getElementById("ingredientListHanger");
	while (myNode2.firstChild) {
	    myNode2.removeChild(myNode2.firstChild);
	}
	
	myNode2 = document.getElementById("updateIngredientListHanger");
	while (myNode2.firstChild) {
	    myNode2.removeChild(myNode2.firstChild);
	}
}

function clearDisplayArea() {
	
	var myNode = document.getElementById("hanger");
	
	while (myNode.firstChild) {
	    myNode.removeChild(myNode.firstChild);
	}
}
	
function removeIngredientBlock(index){
	
	$(".mcc-ingredient-"+index).remove();
}
	
function removeBakeryBlock(index){
	
	$(".mcc-bakery-"+index).remove();
}

function removeRecipeBlock(index){
	
	$(".mcc-recipe-"+index).remove();
}

function getTemplates() {
	
	$.ajax({
		url: navbarTemplateURL
	}).done(function(data){
		$("html").append(data);
		navbarTemplateCompile();
	});
	compileTemplateModal(addIngredientModalTemplateURL,"#addIngredient-template", "Ingredient");
	compileTemplateModal(addIngredientModalTemplateURL,"#updateIngredient-template", "Ingredient");
	compileTemplateModal(addBakeryModalTemplateURL,"#addBakery-template", "Bakery");
	compileTemplateModal(addBakeryModalTemplateURL,"#updateBakery-template", "Bakery");
	compileTemplateModal(addRecipeModalTemplateURL,"#addRecipe-template", "Recipe");
	compileTemplateModal(addRecipeModalTemplateURL,"#updateRecipe-template", "Recipe");
	compileTemplateModal(addRecipeModalTemplateURL,"#salesRecipe-template", "Sales");
	compileTemplateModal(addRecipeModalTemplateURL,"#salesBakery-template", "Sales");
}

function compileTemplateModal(templateURL, templateName, context){
	$.ajax({
		url: templateURL
	}).done(function(data){
		$("html").append(data);
		compileModal(templateName, context);
	});
}

function navbarTemplateCompile(){
	var source   = $("#mcc-navbar-template").html(),
		template = Handlebars.compile(source),
		context = {};
	$('.mcc-navbar-display').append(template(context));
}

function compileModal(template, type){
	var source = $(template).html();
		template = Handlebars.compile(source),
		modalContext = { "title": type},
		context = modalContext,
		compiledHtml = template(context);
	$('.modalZone').append(compiledHtml);
}

function setCostPrecisionIngredients(){
	
	var costList  = $(".mcc-cost-value"),
		thisCost;
	
	$.each(costList, function(){
		thisCost = $(this).text();
		thisCost = Number(thisCost);
		thisCost = thisCost.toFixed(2);
		$(this).text(thisCost);
	});
}

function setCostPrecision(){
	
	var costList  = $(".mcc-ingredient-row"),
		recipeId,
		cost,
		thisCost;
	
	$.each(costList, function(){
		recipeId = $(this).parent().data('id');
		cost = $(this).data("cost");
		thisCost = $(this).children(".mcc-ingredient-cost").text();
		thisCost = Number(thisCost);
		thisCost = thisCost.toFixed(2);
		$(this).children(".mcc-ingredient-cost").text(thisCost);
	});
}

function calculateTotalCost() {
	
	var recipeList = $(".mcc-recipe-block"),
		recipeId,
		cost,
		costs;
		
	$.each(recipeList, function(){
		cost = 0;
		costs = 0;
		recipeId = $(this).data('id');
		$.each($(this).children(".mcc-ingredients-list").children(), function(){
			costs = $(this).data("cost");
			cost = cost + costs;
		});
		cost = Number(cost);
		cost = cost.toFixed(2);
		$(this).children().children(".recipe-total-"+recipeId).text("$"+ cost);
		$(".salesRecipeBtn"+recipeId).data('cost', cost);
	});
}

function compileBlocks (templateSource, blockURL, getURL){
	clearDisplayArea();
	var source,
		template,
		context;
	
	$.ajax({
		url:  blockURL
	}).done(function(data){
		$("html").append(data);
		$.ajax({
			url: getURL
		}).done (function(data){
			source = $(templateSource).html();
			template = Handlebars.compile(source);
			context = data;
			$('.displayZone').append(template(context));
			setCostPrecisionIngredients();
			if(templateSource === "#mcc-recipe-template"){
				calculateTotalCost();
				setCostPrecision();
			}
		});
	});
}



function uniqueValidation(response) {
	var errorArray = response.responseJSON;
	$.each(errorArray, function() {
		var currentError = this.code;
		console.log(currentError);
		if (currentError === 4) {
			errorToast("Combination of ingredient name and unit are not unique.");
		} else {
			errorToast("Database error");
		}
	});
}


function uniqueValidationBakery(response) {
	var errorArray = response.responseJSON;
	$.each(errorArray, function() {
		var currentError = this.code;
		console.log(currentError);
		if (currentError === 4) {
			errorToast("Combination of item name and vendor are not unique.");
		} else {
			errorToast("Database error");
		}
	});
}
