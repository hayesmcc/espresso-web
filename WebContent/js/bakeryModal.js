$(document).ready(function() {

	$(".displayZone").on("click", ".salesBakeryBtn", function() {
		var saleCost = $(this).data('cost'),
			saleName = $(this).data('name');
		saleCost = saleCost.toFixed(2);
		$(".bakeryBaseCostInput").text(saleCost);
		$(".sale-title").text(saleName);
	});

	$($modalZone).on("click", ".bakerySalesCalculateBtn", function() {
		var markup = Number($(".bakerySalesMarkupInput").val()/100),
			bakeryCost = Number($(".bakeryBaseCostInput").text()),
			finalCost = bakeryCost + (bakeryCost * markup);
		finalCost = finalCost.toFixed(2);

		$(".bakerySalesHanger").text("$" + finalCost);
	});
	
	$($modalZone).on("click", ".newBakeryBtn", function() {

		var nameInput = $(".newBakeryNameInput").val(),
			costInput = $(".newBakeryCostInput").val(),
			vendorInput = $(".newBakeryVendorInput").val(),
			categoryInput =  $(".newBakeryCategoryInput").val(),
			allergenInput =  $(".newBakeryAllergenInput").val();
			
		postNewBakery(nameInput, costInput, vendorInput, categoryInput, allergenInput);
	});

	$($modalZone).on("click", ".updateBakeryBtn", function() {
		var updateId = $(".updateBakeryBtn").attr("data-id"),
			nameInput = $(".updateBakeryNameInput").val(),
			costInput = $(".updateBakeryCostInput").val(),
			vendorInput = $(".updateBakeryVendorInput").val(),
			categoryInput =  $(".updateBakeryCategoryInput").val(),
			allergenInput =  $(".updateBakeryAllergenInput").val();
	
		updateBakery(updateId, nameInput, costInput, vendorInput, categoryInput, allergenInput);
	});
	
	$(".displayZone").on("click", ".editBakeryBtn", function() {
		var index = $(this).attr("data-id");
		populateUpdateBakeryModal(index);
	});
	
	$(".displayZone").on("click", ".deleteBakeryBtn", function() {
		var index = $(this).attr("data-id");
		deleteBakery(index);
	});
});

function populateUpdateBakeryModal(index) {
	$(".updateBakeryBtn").attr('data-id', index);   
	$(".updateBakeryNameInput").val($(".mcc-bakery-"+index).children().children(".mcc-name-value").text());
	$(".updateBakeryCostInput").val($(".mcc-bakery-"+index).children(".mcc-cost-value").text());
	$(".updateBakeryVendorInput").val($(".mcc-bakery-"+index).children(".mcc-vendor-value").text());
	$(".updateBakeryCategoryInput").val($(".mcc-bakery-"+index).children(".mcc-category-value").text());
	$(".updateBakeryAllergenInput").val($(".mcc-bakery-"+index).children(".mcc-allergen-value").text());

}

function updateBakeryPage(updateId, nameInput, costInput, vendorInput, categoryInput, allergenInput){
	$(".mcc-bakery-"+updateId).children(".mcc-name-value").text(nameInput);
	$(".mcc-bakery-"+updateId).children(".mcc-cost-value").text(costInput);
	$(".mcc-bakery-"+updateId).children(".mcc-vendor-value").text(vendorInput);
	$(".mcc-bakery-"+updateId).children(".mcc-category-value").text(categoryInput);
	$(".mcc-bakery-"+updateId).children(".mcc-allergen-value").text(allergenInput);
}

function postNewBakery(nameInput, costInput, vendorInput, categoryInput, allergenInput) {
	
	var userObj = {
		"name" : nameInput,
		"cost" : costInput,
		"vendor" : vendorInput,
		"category" : categoryInput,
		"allergen" : allergenInput			
	}, categoryJSON = JSON.stringify(userObj);

	$.ajax({
		url : bakeryURL,
		type : 'POST',
		data : categoryJSON,
		contentType : "application/json"
	}).done(function() {
		$(".addBakeryModal").modal("hide");
		succeessToast('New Bakery Item Added!');
	}).fail(function(response) {
		uniqueValidationBakery(response);
	});
}

function updateBakery (updateId, nameInput, costInput, vendorInput, categoryInput, allergenInput) {
	var userObj = {
		"id" : updateId,
		"name" : nameInput,
		"cost" : costInput,
		"vendor" : vendorInput,
		"category" : categoryInput,
		"allergen" : allergenInput
	}, categoryJSON = JSON.stringify(userObj);
	$.ajax({
		url : bakeryURL,
		type : 'PUT',
		data : categoryJSON,
		contentType : "application/json"
	}).done(function() {
		$(".updateBakeryModal").modal("hide");
		succeessToast('Bakery Updated!');
		updateBakeryPage(updateId, nameInput, costInput, vendorInput, categoryInput, allergenInput);
	}).fail(function(response) {
		errorToast("error");
	});
}

function deleteBakery(index) {

	var userObj = {
		"id" : index
	}, categoryJSON = JSON.stringify(userObj);
	
	$.ajax({
		url: delBakeryURL+index,
		type: 'DELETE'
	}).done(function(data){
		succeessToast('Bakery deleted successfully');
		removeBakeryBlock(index);
	});
}