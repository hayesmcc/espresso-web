var ingredientArray = [],
	updateIngredientArray = [];

$(document).ready(function() {
		
	populateRecipeIngredientSelect();

	$(".displayZone").on("click", ".salesRecipeBtn", function() {
	
		var saleCost = $(this).data('cost'),
			saleName = $(this).data('name');
		$(".baseCostInput").text(saleCost);
		$(".sale-title").text(saleName);
	});
	
	$($modalZone).on("click", ".recipeSalesCalculateBtn", function() {
		var markup = Number($(".salesMarkupInput").val()/100),
			recipeCost = Number($(".baseCostInput").text()),
			finalCost = recipeCost + (recipeCost * markup);
		finalCost = finalCost.toFixed(2);
		$(".salesHanger").text("$" + finalCost);
	});
		
	$($modalZone).on("click", ".updateAddIngredientListBtn", function() {
		
		var inList = false,
			ingredientIndex = Number($( "#updateIngredientOptions option:selected" ).attr("data-id")),
			ingredientId = "mcc-updateIngredientlist-" + ingredientIndex,
			ingredientText = $("<div class = "+ ingredientId + "></div>").text( $("#updateIngredientOptions option:selected" ).text());
		
		for (i = 0; i < updateIngredientArray.length; ++i){
			if (ingredientIndex === updateIngredientArray[i])
				inList = true;
		}
		
		if(!inList){
			updateIngredientArray.push(ingredientIndex);
			$(".updateIngredientListHanger").append(ingredientText);
		}
	});
	
	$($modalZone).on("click", ".updateRemoveIngredientListBtn", function() {
		
		var inList = false,
			ingredientIndex = Number($( "#updateIngredientOptions option:selected" ).attr("data-id")),
			ingredientId = $(".mcc-updateIngredientlist-" + ingredientIndex);
			
		for (i = 0; i < updateIngredientArray.length; ++i){
			if (ingredientIndex === updateIngredientArray[i])
				inList = true;
		}
		if(inList){
			updateIngredientArray.pop(ingredientIndex);
			$(ingredientId).remove();
		}
	});
	
	$($modalZone).on("click", ".addIngredientListBtn", function() {
		
		var inList = false,
			ingredientIndex = $( "#ingredientOptions option:selected" ).attr("data-id"),
			ingredientId = "mcc-ingredientlist-" + ingredientIndex,
			ingredientText = $("<div class = "+ ingredientId + "></div>").text( $("#ingredientOptions option:selected" ).text());
			
		for (i = 0; i < ingredientArray.length; ++i){
			if (ingredientIndex === ingredientArray[i])
				inList = true;
		}
	
		if(!inList){
			ingredientArray.push(ingredientIndex);
			$(".ingredientListHanger").append(ingredientText);
		}		
	});
	
	$($modalZone).on("click", ".removeIngredientListBtn", function() {
		
		var inList = false,
			ingredientIndex = $( "#ingredientOptions option:selected" ).attr("data-id"),
			ingredientId = $(".mcc-ingredientlist-" + ingredientIndex);
			
		for (i = 0; i < ingredientArray.length; ++i){
			if (ingredientIndex === ingredientArray[i])
				inList = true;
		}
		if(inList){
			ingredientArray.pop(ingredientIndex);
			$(ingredientId).remove();
		}
	});
		
	$($modalZone).on("click", ".newRecipeBtn", function() {

		var nameInput = $(".newRecipeNameInput").val(),
			recipeNumberInput =  $(".newRecipeRecipeNumberInput").val();
		
		postNewRecipe(nameInput, recipeNumberInput, ingredientArray);
	});
	
	$($modalZone).on("click", ".updateRecipeBtn", function() {
		var updateId = $(".updateRecipeBtn").attr("data-id"),
			nameInput =  $(".updateRecipeNameInput").val(),
			recipeNumberInput =  $(".updateRecipeRecipeNumberInput").val();
		
		updateRecipe(updateId, nameInput, recipeNumberInput, updateIngredientArray);
	});

	$(".displayZone").on("click", ".editRecipeBtn", function() {
		var index = $(this).attr("data-id");
		populateUpdateRecipeModal(index);
	});
	
	$(".displayZone").on("click", ".deleteRecipeBtn", function() {
		var index = $(this).attr("data-id");
		deleteRecipe(index);
	});
});

function clearIngredientsListArray() {
	
	while(ingredientArray.length > 0) {
		ingredientArray.pop();
	}
	
	while(updateIngredientArray.length > 0) {
		updateIngredientArray.pop();
	}
	
	totalCost = 0;
}

function populateRecipeIngredientSelect(){
	
	var options = "";
	
	$.ajax({
		url: ingredientURL
		}).done (function(data){		
			$.each(data, function(index){
			options+="<option data-id = "+data[index].id+" value='"+ data[index].id+"'>"+ data[index].name+"</option>";
			});
		$("#ingredientOptions").append(options);
		$("#updateIngredientOptions").append(options);
	});
}

function addToIngredientList() {
	var source = $("#mcc-ingredientlist-template").html(), template = Handlebars.compile(source), 
		modalContext = {}, 
		context = modalContext, 
		compiledHtml = template(context);
	$(".ingredientListHanger").append(compiledHtml);
}

function populateUpdateRecipeModal(index) {

	var ingredients = [],
		tempItem,
		tempId = 0,
		ingredientId;
	
	$.ajax({
		url: recipeURL
	}).done(function(data){
	
		$(".updateRecipeBtn").attr('data-id', index);  
	
		for(i = 0; i < data.length; ++i){
			
			if (index == data[i].id ){
				$(".updateRecipeRecipeNumberInput").val(data[i].recipeNumber);
				$(".updateRecipeNameInput").val(data[i].name);
				ingredients = data[i].ingredients;
			
				while(ingredients.length > 0){
					tempItem = ingredients.pop();
					tempId = tempItem.id;
					tempItem = tempItem.name;
					ingredientId = "mcc-updateIngredientlist-" + tempId;
					$(".updateIngredientListHanger").append('<div class = "'+ ingredientId + '">'+ tempItem + '</div>');
					updateIngredientArray.push(tempId);
				}
			}
		}
	});
}

function postNewRecipe(nameInput, recipeNumberInput, ingredientArray) {

	var ValuesArray = [],
    	categoryJSON,
    	userObj,
		object;
	
	for (i = 0; i < ingredientArray.length; ++i){
		object = {"id": ingredientArray[i]}
		ValuesArray.push(object);
	}

	userObj = {
			"name" : nameInput,
			"recipeNumber" : recipeNumberInput,
			"ingredients": ValuesArray
	};

	categoryJSON = JSON.stringify(userObj);

	$.ajax({
		url : recipeURL,
		type : 'POST',
		data : categoryJSON,
		contentType : "application/json"
	}).done(function() {
		$(".addRecipeModal").modal("hide");
		succeessToast('New Recipe Item Added!');
	}).fail(function(response) {
		errorToast("error");
	});
}

function updateRecipe (updateId, nameInput, recipeNumberInput, ingredientArray) {
 
	var ValuesArray = [],
		categoryJSON,
		userObj,
		object;
		
	for (i = 0; i < ingredientArray.length; ++i){
		object = {"id": ingredientArray[i]}
		ValuesArray.push(object);
	}
	userObj = {
		"id" : updateId,
		"name" : nameInput,
		"recipeNumber" : recipeNumberInput,
		"ingredients": ValuesArray
	}, categoryJSON = JSON.stringify(userObj);
	
	$.ajax({
		url : recipeURL,
		type : 'PUT',
		data : categoryJSON,
		contentType : "application/json"
	}).done(function() {
		$(".updateRecipeModal").modal("hide");
		succeessToast('Recipe Updated!');
	}).fail(function(response) {
		errorToast("error");
	});
}

function deleteRecipe(index) {

	var userObj = {
		"id" : index
	}, categoryJSON = JSON.stringify(userObj);
	
	$.ajax({
		url: delRecipeURL+index,
		type: 'DELETE'
	}).done(function(data){
		succeessToast('Recipe deleted successfully');
		removeRecipeBlock(index);
	});
}
