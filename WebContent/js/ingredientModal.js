$(document).ready(function() {
	$($modalZone).on("click", ".addIngredientBtn", function() {

		var nameInput = $(".newIngredientNameInput").val(),
			unitInput = $(".newIngredientUnitInput").val(),
			costInput = $(".newIngredientCostInput").val();
		costInput = Number(costInput);
		costInput = costInput.toFixed(2);
		postNewIngredient(nameInput, unitInput, costInput);
	});
	
	$($modalZone).on("click", ".updateIngredientBtn", function() {
	
		var updateId = $(".updateIngredientBtn").attr("data-id"),
			nameInput = $(".updateIngredientNameInput").val(),
			unitInput = $(".updateIngredientUnitInput").val(),
			costInput = $(".updateIngredientCostInput").val();
		costInput = Number(costInput);
		costInput = costInput.toFixed(2);
		updateIngredient(updateId, nameInput, unitInput, costInput);
	});

	$(".displayZone").on("click", ".editIngredientBtn", function() {
		
		var index = $(this).attr("data-id");
		populateUpdateIngredientModal(index)
	});

	$(".displayZone").on("click", ".deleteIngredientBtn", function() {
		
		var index = $(this).attr("data-id");
		deleteIngredient(index);
	});
});

function updateIngredientPage(updateId, nameInput, unitInput, costInput){

	$(".mcc-ingredient-"+updateId).children().children(".mcc-name-value").text(nameInput);
	$(".mcc-ingredient-"+updateId).children().children(".mcc-unit-value").text(unitInput);
	$(".mcc-ingredient-"+updateId).children().children(".mcc-cost-value").text(costInput);
}

function populateUpdateIngredientModal(index) {

	var modalIndex = index - 1;
	
	$.ajax({
		url: ingredientURL
	}).done(function(data){
		$(".updateIngredientBtn").attr('data-id' , index);   
		$(".updateIngredientNameInput").val($(".mcc-ingredient-"+index).children().children(".mcc-name-value").text());
		$(".updateIngredientUnitInput").val($(".mcc-ingredient-"+index).children().children(".mcc-unit-value").text());
		$(".updateIngredientCostInput").val($(".mcc-ingredient-"+index).children().children(".mcc-cost-value").text());
	});
}

function deleteIngredient(index) {

	userObj = {
		"id" : index
	}, categoryJSON = JSON.stringify(userObj);
	
	$.ajax({
		url: delIngredientURL+index,
		type: 'DELETE'
	}).done(function(data){
		succeessToast('Ingredient deleted successfully');
		removeIngredientBlock(index);
	}).fail(function(data){
		errorToast('This ingredient is currently used in a recipe and cannot be deleted.');
	});
}

function postNewIngredient(nameInput, unitInput, costInput) {
	var userObj = {	
	
		"name" : nameInput,
		"unit" : unitInput,
		"cost" : costInput		
	}, categoryJSON = JSON.stringify(userObj);

	$.ajax({
		url : ingredientURL,
		type : 'POST',
		data : categoryJSON,
		contentType : "application/json"
	}).done(function() {
		$(".addIngredientModal").modal("hide");
		succeessToast('New Ingredient Added!');
	}).fail(function(response) {
		uniqueValidation(response);
	});
}

function updateIngredient(updateId, nameInput, unitInput, costInput) {
	var userObj = {
		"id" : updateId,
		"name" : nameInput,
		"unit" : unitInput,
		"cost" : costInput	
	}, categoryJSON = JSON.stringify(userObj);

	$.ajax({
		url : ingredientURL,
		type : 'PUT',
		data : categoryJSON,
		contentType : "application/json"
	}).done(function() {
		$(".updateIngredientModal").modal("hide");
		succeessToast('Ingredient Updated!');
		updateIngredientPage(updateId, nameInput, unitInput, costInput);
	}).fail(function(response) {
		errorToast("error");
	});
}
