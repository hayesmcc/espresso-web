{
  "src_folders" : ["nightwatch/tests"],
  "output_folder" : "nightwatch/tests/reports",
  "custom_assertions_path" : "",
  "globals_path" : "",
  "live_output" : false,
  "parallel_process_delay" : 10,
  "disable_colors": false,
  "page_objects_path": "nightwatch/PageObjects",

  "selenium" : {
    "start_process" : true,
    "server_path" : "utils/selenium-server-standalone-2.45.0.jar",
    "log_path" : "nightwatch/tests/reports",
    "host" : "127.0.0.1",
    "port" : 4444,
    "cli_args" : {
      "webdriver.chrome.driver" : "c:/tools/chromedriver.exe",
      "webdriver.ie.driver" : "",
      "webdriver.firefox.profile" : ""
    }
  },

  "test_settings" : {
    "default" : {
      "launch_url" : "http://localhost",
      "selenium_host" : "127.0.0.1",
      "selenium_port" : 4444,
      "silent" : true,
      "disable_colors": false,
      "screenshots" : {
        "enabled" : false,
        "path" : ""
      },
      "desiredCapabilities" : {
        "browserName" : "chrome",
        "javascriptEnabled" : true,
        "acceptSslCerts" : true
      }
    }

}
}


