module.exports = function(grunt) {
  
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
            all: {
                files: {
                    'WebContent/js/all.min.js': ['WebContent/js/*.js', '!WebContent/js/all.min.js']
                }
            }
        },
	
	sass: {
            options: {
                style: 'compressed',
                precision: 5
            },
            all: {
                files: {
                    'WebContent/css/index.css': 'WebContent/css/index.scss'
                }
            }
        },
	
    jshint: {
      files: ['Gruntfile.js', 'WebContent/js/*.js', '!WebContent/js/all.min.js', 'WebContent/test/**/*.js'],
      options: {
    	  eqeqeq: true,
        globals: {
          jQuery: true,
		  console: true,
		  module: true,
        },
        laxcomma: true,
		laxbreak: true
      }
    },
	
	watch: {
            javascript: {
                files: ['WebContent/js/*.js', '!WebContent/js/all.min.js'],
                tasks: ['jshint', 'uglify']
            },
            sass: {
                files: ['WebContent/css/*.scss','!WebContent/css/all.min.js'],
                tasks: 'sass'
            },
			html: {
                files: ['WebContent/index.html','Webcontent/**/*.css'],
                options: {
                    livereload: true
                }
			}
        }
  });


  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-sass');

  grunt.registerTask('default', ['watch', 'uglify','jshint','sass']);


};